angular.module('starter')

.config(Routes);

Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

function Routes($stateProvider, $urlRouterProvider) {

  function resolveAuthentication(AuthResolver) { 
    return AuthResolver.resolve();
  }

  $stateProvider
  .state('chat', {
    url: '/chat/:userId',
    reloadOnSearch: false,
    templateUrl: ionic.Platform.isAndroid() ? 'templates/android/chat/chat.tmpl.html' 
               : ionic.Platform.isIOS()     ? 'templates/ios/chat/chat.tmpl.html' 
                                            : 'templates/shared/chat/chat.tmpl.html',
    controller: 'ChatCtrl',
    resolve: {
      auth: resolveAuthentication
    }
  })
  .state('register', {
    url: '/register',
    templateUrl: 'templates/shared/user/register.tmpl.html',
    controller: 'RegisterCtrl'
  })
  .state('login', {
    url: '/login',
    templateUrl: 'templates/shared/user/login.tmpl.html',
    controller: 'LoginCtrl'
  })
  .state('users', {
    url: '/users',
    reloadOnSearch: false,
    templateUrl: 'templates/shared/user/user-list.tmpl.html',
    controller: 'UserListCtrl',
    resolve: {
      auth: resolveAuthentication
    }
  });

  $urlRouterProvider.otherwise('/users');
}

