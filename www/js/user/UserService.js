angular.module('starter')

.service('UserService', UserService);

UserService.$inject = ['$q', '$http', 'UserProvider', 'PARSE_CONFIG'];

function UserService($q, $http, UserProvider, PARSE_CONFIG) {

  function get(options) {
    options || (options = {});

    var deferred = $q.defer();
    var promise = deferred.promise;

    promise.success = function(fn) {
      promise.then(fn);
      return promise;
    };

    promise.error = function(fn) {
      promise.then(null, fn);
      return promise;
    };

    $http({
      method: 'GET',
      url: PARSE_CONFIG.apiBaseUrl + '1/users' + (typeof options.userId === 'string' ? '/' + options.userId : ''),
      headers: {
        "X-Parse-Application-Id": PARSE_CONFIG.appId,
        "X-Parse-REST-API-Key": PARSE_CONFIG.apiKey,
      },
      params: options.options
    })
    .then(
    function success(response) {
      if(typeof options.userId === 'string') {
        var singleResult = response.data;
        deferred.resolve(singleResult);
        UserProvider.containsUser(singleResult)
        .then(function(result) {
          if(!result.result) {
            UserProvider.storeUser(result.userObject);
          }
        });
      } else {
        var results = response.data.results;
        deferred.resolve(results);
        for(var i = 0, length = results.length; i < length; ++i) {
          UserProvider.containsUser(results[i])
          .then(function(result) {
            if(!result.result) {
              UserProvider.storeUser(result.userObject);
            }
          });
        }
      }
    },
    function error(response) {
      deferred.reject({
        code: response.data ? response.data.code : null,
        message: response.data ? response.data.error : 'Could not connect with the server'
      });
    });

    return promise;
  }

  function getAllUsers() {
    return get().then(function success(results) {
      return results;
    }, function error(error) {
      return UserProvider.getAllUsers();
    });
  }

  function getUser(userId) {
    return get({ userId: userId }).then(function success(results) {
      return results;
    }, function error(error) {
      return UserProvider.getAllUsers().then(function(users) {
        var user = users.filter(function(user) { return user.objectId === userId; })
        return user.length > 0 ? user[0] : null;
      });
    });
  }

  return {
    getAllUsers: getAllUsers,
    getUser: getUser,
  };
}

