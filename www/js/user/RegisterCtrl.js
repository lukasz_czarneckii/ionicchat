angular.module('starter')

.controller('RegisterCtrl', RegisterCtrl);

RegisterCtrl.$inject = ['$scope', 'AuthService', '$ionicPopup', '$state', '$ionicLoading', '$ionicHistory'];

function RegisterCtrl($scope, AuthService, $ionicPopup, $state, $ionicLoading, $ionicHistory) {
  $scope.data = {};

  $scope.register = function() {
    $ionicLoading.show({
      template: 'Registering...'
    });

    AuthService.registerUser($scope.data)
    .then(function success(data) {
      // remove login and register views from history
      $ionicHistory.currentView($ionicHistory.goBack(-2));
      $state.go('users', null, { location: 'replace' });
    },
    function error(data) {
      var alertPopup = $ionicPopup.alert({
        title: 'Sign up failed!',
        template: 'Error: ' + data.error.message
      });
    })
    .finally(function() {
      $ionicLoading.hide();
    });
  };
}

