angular.module('starter')

.factory('AuthResolver', AuthResolver);

AuthResolver.$inject = ['$q', 'AuthService', '$state'];

function AuthResolver($q, AuthService, $state) {
  return {
    resolve: function() {
      var deferred = $q.defer();

      AuthService.currentUser()
      .then(
      function success() {
        deferred.resolve();
      },
      function error() {
        deferred.reject();
        $state.go('login');
      });
      
      return deferred.promise;
    }
  };
}