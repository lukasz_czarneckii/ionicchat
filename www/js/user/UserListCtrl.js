angular.module('starter')

.controller('UserListCtrl', UserListCtrl);

UserListCtrl.$inject = ['$q', '$scope', '$state', '$ionicLoading', 'AuthService', '$ionicHistory', '$ionicPopup', 'UserService'];

function UserListCtrl($q, $scope, $state, $ionicLoading, AuthService, $ionicHistory, $ionicPopup, UserService) {

  $ionicLoading.show({
    template: 'Loading users...'
  });

  $scope.$on('$ionicView.enter', function() {
    $q.all([UserService.getAllUsers(), AuthService.currentUser()])
    .then(
    function success(results) {
      var users = results[0];
      var currentUser = results[1];

      $scope.users = users.filter(function(user) { return user.objectId !== currentUser.objectId });
      $scope.currentUser = currentUser;
    },
    function error(error) {
      var alertPopup = $ionicPopup.alert({
        title: 'Could not retrieve user data!',
      });
    })
    .finally(function() { 
      $ionicLoading.hide();
    });
  });

  $scope.logout = function() {
    $ionicLoading.show({
      template: 'Logging out...'
    });

    AuthService.logoutUser()
    .finally(function() {
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go('login', null, { location: 'replace' })
      .finally(function() { 
        $ionicHistory.clearHistory();
      });
      
      $ionicLoading.hide();
    });
  };
}

