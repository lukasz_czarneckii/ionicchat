angular.module('starter')

.controller('LoginCtrl', LoginCtrl);

LoginCtrl.$inject = ['$scope', 'AuthService', '$ionicPopup', '$state', '$ionicLoading', '$ionicHistory'];

function LoginCtrl($scope, AuthService, $ionicPopup, $state, $ionicLoading, $ionicHistory) {
  $scope.data = {};

  $scope.login = function() {
    $ionicLoading.show({
      template: 'Authenticating...'
    });

    AuthService.loginUser($scope.data)
    .then(function success(data) {
      // remove login view from history
      $ionicHistory.currentView($ionicHistory.backView());
      $state.go('users', null, { location: 'replace' });
    },
    function error(data) {
      var alertPopup = $ionicPopup.alert({
        title: 'Login failed!',
        template: 'Error: ' + data.error.message
      });
    })
    .finally(function() {
      $ionicLoading.hide();
    });
  };
}

