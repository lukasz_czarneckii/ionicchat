angular.module('starter')

.service('AuthService', AuthService);

AuthService.$inject = ['$q', '$http', 'PARSE_CONFIG', '$localstorage'];

function AuthService($q, $http, PARSE_CONFIG, $localstorage) {
  
  var _sessionToken = undefined;
  var _currentUser = undefined;

  function _createDeferredObject() {
    var deferred = $q.defer();
    var promise = deferred.promise;

    promise.success = function(fn) {
      promise.then(fn);
      return promise;
    };

    promise.error = function(fn) {
      promise.then(null, fn);
      return promise;
    };

    return deferred;
  }

  function _setSessionToken(value) {
    $localstorage.set('_sessionToken', value);
    _sessionToken = value;
  }

  function _getSessionToken() {
    return (_sessionToken || $localstorage.get('_sessionToken') || undefined);
  }

  function _setCurrentUser(object) {
    $localstorage.setObject('_currentUser', object);
    _currentUser = object;
  }

  function _getCurrentUser() {
    return (_currentUser || $localstorage.getObject('_currentUser') || undefined);
  }

  function _clearUserData() {
    $localstorage.remove('_sessionToken');
    $localstorage.remove('_currentUser');
    _sessionToken = undefined;
    _currentUser = undefined;
  }

  function isAuthenticated() {
    return angular.isDefined(_currentUser);
  }

  function currentUser() {
    _sessionToken || (_sessionToken = _getSessionToken());
    _currentUser || (_currentUser = _getCurrentUser());

    var deferred = _createDeferredObject();
    var promise = deferred.promise;

    if(angular.isDefined(_currentUser)) {
      deferred.resolve(_currentUser);
    } else if(!angular.isDefined(_sessionToken)) {
      deferred.reject();
    } else {
      $http({
        method: 'GET',
        url: PARSE_CONFIG.apiBaseUrl + '1/users/me',
        headers: {
          "X-Parse-Application-Id": PARSE_CONFIG.appId,
          "X-Parse-REST-API-Key": PARSE_CONFIG.apiKey,
          "X-Parse-Session-Token": _sessionToken, 
          "Content-Type": "application/json"
        }
      })
      .then(
      function success(response) {
        _setSessionToken(response.data.sessionToken);
        _setCurrentUser(response.data);

        deferred.resolve(response.data);
      },
      function error(response) {
        deferred.reject({ 
          error: {
            code: response.data ? response.data.code : null,
            message: response.data ? response.data.error : 'Could not connect with the server'
          } 
        });
      });
    }

    return promise;
  }

  function loginUser(userData) {
    var deferred = _createDeferredObject();
    var promise = deferred.promise;

    $http({
      method: 'GET',
      url: PARSE_CONFIG.apiBaseUrl + '1/login',
      headers: {
        "X-Parse-Application-Id": PARSE_CONFIG.appId,
        "X-Parse-REST-API-Key": PARSE_CONFIG.apiKey,
        "X-Parse-Revocable-Session": 1, 
        "Content-Type": "application/json"
      },
      params: {
        username: userData.email,
        password: userData.password
      }
    })
    .then(
    function success(response) {
      _setSessionToken(response.data.sessionToken);
      _setCurrentUser(response.data);

      deferred.resolve({ 
        user: response.data, 
        message: 'Authentication success' 
      });
    },
    function error(response) {
      deferred.reject({ 
        user: response.config.data, 
        error: {
          code: response.data ? response.data.code : null,
          message: response.data ? response.data.error : 'Could not connect with the server'
        } 
      });
    });

    return promise;
  }

  function logoutUser() {
    var deferred = _createDeferredObject();
    var promise = deferred.promise;

    $http({
      method: 'POST',
      url: PARSE_CONFIG.apiBaseUrl + '1/logout',
      headers: {
        "X-Parse-Application-Id": PARSE_CONFIG.appId,
        "X-Parse-REST-API-Key": PARSE_CONFIG.apiKey,
        "X-Parse-Session-Token": _sessionToken
      }
    })
    .then(
    function success(response) {
      deferred.resolve();
    },
    function error(response) {
      deferred.reject({
        code: response.data ? response.data.code : null,
        message: response.data ? response.data.error : 'Could not connect with the server'
      });
    })
    .finally(function() {
      _clearUserData();
    });

    return promise;
  }

  function registerUser(userData) {
    var deferred = _createDeferredObject();
    var promise = deferred.promise;

    $http({
      method: 'POST',
      url: PARSE_CONFIG.apiBaseUrl + '1/users',
      headers: {
        "X-Parse-Application-Id": PARSE_CONFIG.appId,
        "X-Parse-REST-API-Key": PARSE_CONFIG.apiKey,
        "Content-Type": "application/json"
      },
      data: {
        username: userData.email,
        email: userData.email,
        firstname: userData.firstname,
        lastname: userData.lastname,
        password: userData.password1
      }
    })
    .then(
    function success(response) {
      _setSessionToken(response.data.sessionToken);
      _setCurrentUser(response.data);

      deferred.resolve({ 
        user: response.config.data, 
        message: 'Your account has been created' 
      });
    },
    function error(response) {
      deferred.reject({ 
        user: response.config.data, 
        error: {
          code: response.data ? response.data.code : null,
          message: response.data ? response.data.error : 'Could not connect with the server'
        } 
      });
    });

    return promise;
  }

  return {
    isAuthenticated: isAuthenticated,
    currentUser: currentUser,
    loginUser: loginUser,
    logoutUser: logoutUser,
    registerUser: registerUser,
  };
}