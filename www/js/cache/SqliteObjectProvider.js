angular.module('starter')

.service('SqliteObjectProvider', SqliteObjectProvider)

SqliteObjectProvider.$inject = ['$q', '$cordovaSQLite', '$ionicPlatform', 'SQLITE_TABLES', 'SQLITE_DB_CONFIG'];

function SqliteObjectProvider($q, $cordovaSQLite, $ionicPlatform, SQLITE_TABLES, SQLITE_DB) {

  var db;
  var isDbOpenPromiseResolved = false;
  var dbOpenPromise;

  function init(options) {
    dbOpenPromise = _openDb(options).then(function success(result) { 
      db = result;
      isDbOpenPromiseResolved = true;
    }, function error(error) {
      console.log(error);
    })
    .then(function success() {
      var promises = [];
      for(var table in SQLITE_TABLES) {
        if(SQLITE_TABLES.hasOwnProperty(table)) {
          promises.push(_createTable(table));
        }
      }
      return $q.all(promises);
    });
    return dbOpenPromise;
  }

  function _createTable(table) {
    return $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS " + table + " (objectId TEXT, object TEXT)", []);
  }

  function _insert(object, table) {
    return $cordovaSQLite.execute(db, "INSERT INTO " + table + " VALUES (?, ?)", [object.objectId, JSON.stringify(object)]);
  }

  function _select(object, table) {
    var query = "SELECT * FROM " + table + (typeof object !== 'undefined' ? " WHERE objectId = ?" : "");
    var bindings = typeof object !== 'undefined' ? [object.objectId] : [];

    return $cordovaSQLite.execute(db, query, bindings)
    .then(function(results) {
      var objects = [];
      for(var i = 0, length = results.rows.length; i < length; ++i)
        objects.push(JSON.parse(results.rows.item(i).object));
      return objects;
    });
  }

  function _remove(object, table) {
    return object ? $cordovaSQLite.execute(db, "DELETE FROM " + table + " WHERE objectId = ?", [object.objectId])
                  : $cordovaSQLite.execute(db, "DELETE FROM " + table, []);
  }

  function _openDb(options) {
    if(isDbOpenPromiseResolved) {
      return $q.when(db);
    } else {
      return $ionicPlatform.ready().then(function() {
        options || (options = { });
        options.name = options.name || SQLITE_DB.name;

        db = window.sqlitePlugin.openDatabase(options);
        return db;
      });
    }
  }

  function store(objects, table) {
    return $q.when(dbOpenPromise ? dbOpenPromise : init()).then(function() {
      if(!(objects instanceof Array)) {
        return _insert(objects, table);
      }

      var promises = [];
      for(var i = 0, length = objects.length; i < length; ++i) {
        promises.push(_insert(objects[i], table));
      }

      return $q.all(promises);
    });
  }

  function get(options, table) {
    return $q.when(dbOpenPromise ? dbOpenPromise : init()).then(function() {
      // get all objects
      return _select(undefined, table);
    })
    .then(function(objects) {
      // filter according to options
      return objects.filter(function(object) {
        for(property in options) {
          if(options.hasOwnProperty(property) && options[property] !== object[property])
            return false;
        }
        return true;
      });
    });
  }

  function contains(object, table) {
    return $q.when(dbOpenPromise ? dbOpenPromise : init()).then(function() {
      return _select(object, table);
    })
    .then(function(objects) {
      return objects.length > 0;
    });
  }

  function remove(objects, table) {
    return $q.when(dbOpenPromise ? dbOpenPromise : init()).then(function() {
      if(!(objects instanceof Array)) {
        return _remove(objects, table);
      }

      var promises = [];
      for(var i = 0, length = objects.length; i < length; ++i) {
        promises.push(_remove(objects[i], table));
      }

      return $q.all(promises);
    });
  }

  function clear(table) {
    return $q.when(dbOpenPromise ? dbOpenPromise : init()).then(function() {
      return _remove(undefined, table);
    });
  }

  return {
    init: init,

    store: store,
    get: get,
    contains: contains,
    remove: remove,
    clear: clear,
  };
}