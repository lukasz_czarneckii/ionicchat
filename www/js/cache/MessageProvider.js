angular.module('starter')

.service('MessageProvider', MessageProvider)

MessageProvider.$inject = ['SqliteObjectProvider', 'SQLITE_TABLES'];
// MessageProvider.$inject = ['LocalStorageObjectProvider', 'LOCAL_STORAGE_KEY_PREFIXES'];

function MessageProvider(ObjectProvider, STORAGE_CONFIG) {

  function storeMessage(message) {
    return ObjectProvider.store(message, STORAGE_CONFIG.messages);
  }

  function storeMessages(messages) {
    return ObjectProvider.store(messages, STORAGE_CONFIG.messages);
  }

  function getAllMessages() {
    return ObjectProvider.get(undefined, STORAGE_CONFIG.messages);
  }

  function getMessagesForChat(chatId) {
    return ObjectProvider.get({ chatId: chatId }, STORAGE_CONFIG.messages );
  }

  function containsMessage(message) {
    return ObjectProvider.contains(message, STORAGE_CONFIG.messages).then(function(result){
      return {
        result: result,
        messageObject: message,
      };
    });
  }

  function removeMessageFromStorage(message) {
    return ObjectProvider.remove(message, STORAGE_CONFIG.messages);
  }

  function removeMessagesFromStorage(messages) {
    return ObjectProvider.remove(messages, STORAGE_CONFIG.messages);
  }

  function removeAllMessagesFromStorage() {
    return ObjectProvider.clear(STORAGE_CONFIG.messages);
  }

  function removeChatMessagesFromStorage(chatId) {
    return getMessagesForChat(chatId).then(function(messages) {
      return removeMessagesFromStorage(messages);
    });
  }

  return {
    storeMessage: storeMessage,
    storeMessages: storeMessages,
    getAllMessages: getAllMessages,
    getMessagesForChat: getMessagesForChat,
    containsMessage: containsMessage,
    removeMessageFromStorage: removeMessageFromStorage,
    removeMessagesFromStorage: removeMessagesFromStorage,
    removeAllMessagesFromStorage: removeAllMessagesFromStorage,
    removeChatMessagesFromStorage: removeChatMessagesFromStorage,
  };
}

