angular.module('starter')

.service('ChatProvider', ChatProvider)

ChatProvider.$inject = ['SqliteObjectProvider', 'SQLITE_TABLES'];
// ChatProvider.$inject = ['LocalStorageObjectProvider', 'LOCAL_STORAGE_KEY_PREFIXES'];

function ChatProvider(ObjectProvider, STORAGE_CONFIG) {

  function storeChat(chat) {
    return ObjectProvider.store(chat, STORAGE_CONFIG.chats);
  }

  function storeChats(chats) {
    return ObjectProvider.store(chats, STORAGE_CONFIG.chats);
  }

  function getAllChats() {
    return ObjectProvider.get(undefined, STORAGE_CONFIG.chats);
  }

  function getAllUserChats(userId) {
    return getAllChats().then(function(chats) {
      return chats.filter(function(chat) {
        return chat.users.indexOf(userId) !== -1;
      });
    });
  }

  function getUsersChat(userIds) {
    return getAllChats().then(function(chats) {
      var userIdsAsString = userIds.sort().join(' ');
      return chats.filter(function(chat) {
        return chat.users.sort().join(' ') === userIdsAsString;
      });
    });
  }

  function containsChat(chat) {
    return ObjectProvider.contains(chat, STORAGE_CONFIG.chats).then(function(result){
      return {
        result: result,
        chatObject: chat,
      };
    });
  }

  function removeChatFromStorage(chat) {
    return ObjectProvider.remove(chat, STORAGE_CONFIG.chats);
  }

  function removeChatsFromStorage(chats) {
    return ObjectProvider.remove(chats, STORAGE_CONFIG.chats);
  }

  function removeAllChatsFromStorage() {
    return ObjectProvider.clear(STORAGE_CONFIG.chats);
  }

  return {
    storeChat: storeChat,
    storeChats: storeChats,
    getAllChats: getAllChats,
    getAllUserChats: getAllUserChats,
    getUsersChat: getUsersChat,
    containsChat: containsChat,
    removeChatFromStorage: removeChatFromStorage,
    removeChatsFromStorage: removeChatsFromStorage,
    removeAllChatsFromStorage: removeAllChatsFromStorage,
  };
}

