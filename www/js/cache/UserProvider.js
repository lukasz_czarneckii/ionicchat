angular.module('starter')

.service('UserProvider', UserProvider)

UserProvider.$inject = ['SqliteObjectProvider', 'SQLITE_TABLES'];
// UserProvider.$inject = ['LocalStorageObjectProvider', 'LOCAL_STORAGE_KEY_PREFIXES'];

function UserProvider(ObjectProvider, STORAGE_CONFIG) {

  function storeUser(user) {
    return ObjectProvider.store(user, STORAGE_CONFIG.users);
  }

  function storeUsers(users) {
    return ObjectProvider.store(users, STORAGE_CONFIG.users);
  }

  function getAllUsers() {
    return ObjectProvider.get(undefined, STORAGE_CONFIG.users);
  }

  function getUsersForChat(chatId) {
    return ObjectProvider.get({ chatId: chatId }, STORAGE_CONFIG.users);
  }

  function containsUser(user) {
    return ObjectProvider.contains(user, STORAGE_CONFIG.users).then(function(result){
      return {
        result: result,
        userObject: user,
      };
    });
  }

  function removeUserFromStorage(user) {
    return ObjectProvider.remove(user, STORAGE_CONFIG.users);
  }

  function removeUsersFromStorage(users) {
    return ObjectProvider.remove(users, STORAGE_CONFIG.users);
  }

  function removeAllUsersFromStorage() {
    return ObjectProvider.clear(STORAGE_CONFIG.users);
  }

  return {
    storeUser: storeUser,
    storeUsers: storeUsers,
    getAllUsers: getAllUsers,
    getUsersForChat: getUsersForChat,
    containsUser: containsUser,
    removeUserFromStorage: removeUserFromStorage,
    removeUsersFromStorage: removeUsersFromStorage,
    removeAllUsersFromStorage: removeAllUsersFromStorage,
  };
}

