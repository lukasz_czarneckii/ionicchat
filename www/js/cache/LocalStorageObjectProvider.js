angular.module('starter')

.service('LocalStorageObjectProvider', LocalStorageObjectProvider)

LocalStorageObjectProvider.$inject = ['$q', '$localstorage'];

function LocalStorageObjectProvider($q, $localstorage) {

  function _createDeferred() {
    var deferred = $q.defer();
    var promise = deferred.promise;

    promise.success = function(fn) {
      promise.then(fn);
      return promise;
    };

    promise.error = function(fn) {
      promise.then(null, fn);
      return promise;
    };

    return deferred;
  }

  function store(objects, keyPrefix) {
    if(!(objects instanceof Array)) {
      // store single object
      return $q.when($localstorage.setObject(keyPrefix + objects.objectId, objects));
    }

    var promises = [];
    for(var i = 0, length = objects.length; i < length; ++i) {
      promises.push($q.when($localstorage.setObject(keyPrefix + objects[i].objectId, objects[i])));
    }
    return $q.all(promises);
  }

  function get(options, keyPrefix) {
    options || (options = {});

    // get all objects
    var all = $localstorage.getObjectsWithKeyPrefix(keyPrefix);

    // filter according to options
    return $q.when(all.filter(function(object) {
      for(property in options) {
        if(options.hasOwnProperty(property) && options[property] !== object[property])
          return false;
      }
      return true;
    }));
  }

  function contains(object, keyPrefix) {
    return $q.when(!!$localstorage.getObject(keyPrefix + object.objectId));
  }

  function remove(objects, keyPrefix) {
    if(!(objects instanceof Array)) {
      return $q.when($localstorage.remove(keyPrefix + objects.objectId));
    }

    var promises = [];
    for(var i = 0, length = objects.length; i < length; ++i) {
      promises.push($q.when($localstorage.remove(keyPrefix + objects[i].objectId)));
    }

    return $q.all(promises);
  }

  function clear(keyPrefix) {
    return get(undefined, keyPrefix).then(function(objects) {
      return remove(objects, keyPrefix);
    });
  }

  return {
    store: store,
    get: get,
    contains: contains,
    remove: remove,
    clear: clear,
  };
}