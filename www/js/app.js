// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ionic.utils', 'ngMessages', 'ngCordova'])

.run(RunApp);

RunApp.$inject = ['$ionicPlatform', 'SqliteObjectProvider', 'PARSE_CONFIG'];

function RunApp($ionicPlatform, SqliteObjectProvider, PARSE_CONFIG) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.disableScroll(true);
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    Parse.initialize(PARSE_CONFIG.appId, PARSE_CONFIG.jsKey);

    // uncomment this line if you plan on using sqlite database
    SqliteObjectProvider.init(ionic.Platform.isAndroid() ? { location: 2, androidDatabaseImplementation: 2 } : undefined);
  });
}

