angular.module('starter')

.service('MessageService', MessageService);

MessageService.$inject = ['$q', '$http', 'MessageProvider', 'PARSE_CONFIG'];

function MessageService($q, $http, MessageProvider, PARSE_CONFIG) {

  function _createDeferred() {
    var deferred = $q.defer();
    var promise = deferred.promise;

    promise.success = function(fn) {
      promise.then(fn);
      return promise;
    };

    promise.error = function(fn) {
      promise.then(null, fn);
      return promise;
    };

    return deferred;
  }

  function get(options) {
    options || (options = {});

    var deferred = _createDeferred();
    var promise = deferred.promise;

    $http({
      method: 'GET',
      url: PARSE_CONFIG.apiBaseUrl + '1/classes/Message' + (typeof options.messageId === 'string' ? '/' + options.messageId : ''),
      headers: {
        "X-Parse-Application-Id": PARSE_CONFIG.appId,
        "X-Parse-REST-API-Key": PARSE_CONFIG.apiKey,
      },
      params: options.options
    })
    .then(
    function success(response) {
      if(typeof options.messageId === 'string') {
        var singleResult = response.data;
        deferred.resolve(singleResult);
        MessageProvider.containsMessage(singleResult)
        .then(function(result) {
          if(!result.result) {
            MessageProvider.storeMessage(result.messageObject);
          }
        });
      } else {
        var results = response.data.results;
        deferred.resolve(results);
        for(var i = 0, length = results.length; i < length; ++i) {
          MessageProvider.containsMessage(results[i])
          .then(function(result) {
            if(!result.result) {
              MessageProvider.storeMessage(result.messageObject);
            }
          });
        }
      }
    },
    function error(response) {
      deferred.reject({
        code: response.data ? response.data.code : null,
        message: response.data ? response.data.error : 'Could not connect with the server'
      });
    });

    return promise;
  }

  function create(messageContent, userId, chatId) {
    var deferred = _createDeferred();
    var promise = deferred.promise;

    $http({
      method: 'POST',
      url: PARSE_CONFIG.apiBaseUrl + '1/classes/Message',
      headers: {
        "X-Parse-Application-Id": PARSE_CONFIG.appId,
        "X-Parse-REST-API-Key": PARSE_CONFIG.apiKey,
        "Content-Type": 'application/json',
      },
      data: { 
        content: messageContent,
        userId: userId,
        chatId: chatId,
      }
    })
    .then(
    function success(response) {
      var message = angular.extend(response.data, response.config.data);
      deferred.resolve(message);
      MessageProvider.storeMessage(message);
    },
    function error(response) {
      deferred.reject({
        code: response.data.code,
        message: response.data.error
      });
    });

    return promise;
  }

  function getMessagesForChat(chatId, messagesLimit) {
    return get({
      options: {
        where: {
          chatId: chatId,
        },
        order: 'createdAt',
        limit: messagesLimit,
      }
    }).then(function success(results) {
      return results;
    }, function error(error) {
      return MessageProvider.getMessagesForChat(chatId);
    });
  }

  function getMessagesForChatAfterTimestamp(chatId, timestamp) {
    return get({
      options: {
        where: {
          chatId: chatId,
          createdAt: {
            '$gt': timestamp
          },
        },
        order: 'createdAt',
      }
    }).then(function success(results) {
      return results;
    }, function error(error) {
      return MessageProvider.getMessagesForChat(chatId)
      .then(function(messages) {
        return messages.filter(function(message) {
          return new Date(message.createdAt).getTime() > new Date(timestamp);
        });
      });
    });
  }

  function getLastMessageForChat(chatId) {
    return get({
      options: {
        where: {
          chatId: chatId,
        },
        order: '-createdAt',
        limit: 1,
      }
    }).then(function success(results) {
      return results;
    }, function error(error) {
      return MessageProvider.getMessagesForChat(chatId).then(function(messages) {
        return messages.length > 0 ? messages[messages.length - 1] : null;
      });
    });
  }

  function sendMessage(message) {
    return create(message.content, message.userId, message.chatId);
  }

  return {
    getMessagesForChat: getMessagesForChat,
    getMessagesForChatAfterTimestamp: getMessagesForChatAfterTimestamp,
    getLastMessageForChat: getLastMessageForChat,
    sendMessage: sendMessage,
  };
}

