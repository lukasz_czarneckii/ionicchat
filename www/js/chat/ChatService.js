angular.module('starter')

.service('ChatService', ChatService);

ChatService.$inject = ['$q', '$http', 'ChatProvider', 'PARSE_CONFIG'];

function ChatService($q, $http, ChatProvider, PARSE_CONFIG) {

  function _createDeferred() {
    var deferred = $q.defer();
    var promise = deferred.promise;

    promise.success = function(fn) {
      promise.then(fn);
      return promise;
    };

    promise.error = function(fn) {
      promise.then(null, fn);
      return promise;
    };

    return deferred;
  }

  function get(options) {
    options || (options = {});

    var deferred = _createDeferred();
    var promise = deferred.promise;

    $http({
      method: 'GET',
      url: PARSE_CONFIG.apiBaseUrl + '1/classes/Chat' + (typeof options.chatId === 'string' ? '/' + options.chatId : ''),
      headers: {
        "X-Parse-Application-Id": PARSE_CONFIG.appId,
        "X-Parse-REST-API-Key": PARSE_CONFIG.apiKey,
      },
      params: options.options
    })
    .then(
    function success(response) {
      if(typeof options.chatId === 'string') {
        var singleResult = response.data;
        deferred.resolve(singleResult);
        ChatProvider.containsChat(singleResult)
        .then(function(result) {
          if(!result.result) {
            ChatProvider.storeChat(result.chatObject);
          }
        });
      } else {
        var results = response.data.results;
        deferred.resolve(results);
        for(var i = 0, length = results.length; i < length; ++i) {
          ChatProvider.containsChat(results[i])
          .then(function(result) {
            if(!result.result) {
              ChatProvider.storeChat(result.chatObject);
            }
          });
        }
      }
    },
    function error(response) {
      deferred.reject({
        code: response.data ? response.data.code : null,
        message: response.data ? response.data.error : 'Could not connect with the server'
      });
    });

    return promise;
  }

  function createChat(userIds, message) {
    var deferred = _createDeferred();
    var promise = deferred.promise;

    $http({
      method: 'POST',
      url: PARSE_CONFIG.apiBaseUrl + '1/classes/Chat',
      headers: {
        "X-Parse-Application-Id": PARSE_CONFIG.appId,
        "X-Parse-REST-API-Key": PARSE_CONFIG.apiKey,
        "Content-Type": 'application/json',
      },
      data: { 
        users: userIds,
      }
    })
    .then(
    function success(response) {
      var chat = angular.extend(response.data, response.config.data);
      deferred.resolve(chat);
      ChatProvider.storeChat(chat);
    },
    function error(response) {
      deferred.reject({
        code: response.data ? response.data.code : null,
        message: response.data ? response.data.error : 'Could not connect with the server'
      });
    });

    return promise;
  }

  function getAllUserChats(userId) {
    return get({
      options: {
        where: {
          users: userId,
        }
      }
    }).then(function success(results) {
      return results;
    }, function error(error) {
      return ChatProvider.getAllUserChats(userId);
    });
  }

  function getChat(userIds) {
    return get({
      options: {
        where: {
          users: { 
            $all: userIds 
          },
        }
      }
    }).then(function success(results) {
      return results;
    }, function error(error) {
      return ChatProvider.getUsersChat(userIds);
    });
  }

  return {
    createChat: createChat,
    getAllUserChats: getAllUserChats,
    getChat: getChat,
  };
}