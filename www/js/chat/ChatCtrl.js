angular.module('starter')

.controller('ChatCtrl', ChatCtrl);

ChatCtrl.$inject = ['$q', '$scope', '$stateParams', '$timeout', '$interval', '$ionicScrollDelegate', '$ionicPopup', '$cordovaNetwork', 'AuthService', 'MessageService', 'ChatService', 'UserService'];

function ChatCtrl($q, $scope, $stateParams, $timeout, $interval, $ionicScrollDelegate, $ionicPopup, $cordovaNetwork, AuthService, MessageService, ChatService, UserService) {

  $scope.sendMessage = function() {
    if($scope.data.message) {
      if($cordovaNetwork.isOffline()) {
        return $ionicPopup.alert({
          title: 'You are offline!',
        });
      }

      var msg = {
        userId: $scope.currentUser.objectId,
        content: $scope.data.message,
        chatId: $scope.currentChat.objectId,
      };

      MessageService.sendMessage(msg)
      .then(function(message) {
        var idx = $scope.messages.indexOf(msg);
        idx !== -1 && $scope.messages.splice($scope.messages.indexOf(msg), 1, message);
      });

      $scope.messages.push(msg);

      delete $scope.data.message;
      $ionicScrollDelegate.scrollBottom(true);
    }
  };

  var scrollBottomPromise;

  $scope.inputUp = function() {
    scrollBottomPromise = $timeout(function() {
      $ionicScrollDelegate.scrollBottom(true);
    }, $scope.scrollDelay);
  };

  $scope.inputDown = function() {
    scrollBottomPromise = $timeout(function() {
      $ionicScrollDelegate.resize();
    }, $scope.scrollDelay);
  };

  function getAllMessagesForChatAndStartPolling() {
    $scope.currentChat && MessageService.getMessagesForChat($scope.currentChat.objectId)
    .then(function(messages) { 
      if(!messages instanceof Array)
        messages = [messages];

      $scope.messages = messages || [];

      startPolling();
    });
  }

  var pollingPromise;

  function startPolling() {
    !angular.isDefined(pollingPromise) && (pollingPromise = $interval(pollForNewMessages, 500));
  }

  function stopPolling() {
    angular.isDefined(pollingPromise) && $interval.cancel(pollingPromise);
  }

  function _getLastOtherUserMessage() {
    for(var i = $scope.messages.length - 1; i >= 0; --i) {
      var msg = $scope.messages[i];
      if(msg.userId !== $scope.currentUser.objectId) {
        return msg;
      }
    }
    return null;
  }

  var isPollingRequestPending = false;

  function pollForNewMessages() {
    if(!isPollingRequestPending) {
      var message = _getLastOtherUserMessage();

      MessageService.getMessagesForChatAfterTimestamp($scope.currentChat.objectId, message ? new Date(Date.parse(message.createdAt)) : new Date(0))
      .then(function(messages) {
        var newMessages = messages.filter(function(msg) {
          return msg.userId !== $scope.currentUser.objectId && $scope.messages.indexOf(msg) == -1;
        });

        $scope.messages.push.apply($scope.messages, newMessages);
      })
      .finally(function() {
        isPollingRequestPending = false;
      });

      isPollingRequestPending = true;
    }
  }

  $scope.data = {};

  $scope.$on('$ionicView.enter', function() {
    $q.all([AuthService.currentUser(), UserService.getUser($stateParams.userId)])
    .then(function(results) {
      $scope.currentUser = results[0];
      $scope.otherUser = results[1];
      return ChatService.getChat([$scope.currentUser.objectId, $scope.otherUser.objectId])
    })
    .then(function(chats) {
      return chats.length > 0 ? $q.when(chats[0]) : ChatService.createChat([$scope.currentUser.objectId, $scope.otherUser.objectId]);
    })
    .then(function(chat) {
      $scope.currentChat = chat;
      getAllMessagesForChatAndStartPolling();
    });

    // scroll to bottom when messages array changes (most of the time, when new messages are added)
    $scope.$watchCollection('messages', function() {
      $timeout(function() {
        $ionicScrollDelegate.scrollBottom(true);
      }, 100);
    });

    if(ionic.Platform.isAndroid()) {
      window.addEventListener('native.keyboardshow', $scope.inputUp);
      window.addEventListener('native.keyboardhide', $scope.inputUp);
      $scope.scrollDelay = 500;
    }

    if(ionic.Platform.isIOS()) {
      $scope.scrollDelay = 100;
    }
  });

  $scope.$on('$ionicView.beforeLeave', function() {
    // Make sure that polling is stopped
    stopPolling();
    
    // Reject deferred scroll to bottom
    scrollBottomPromise && $timeout.cancel(scrollBottomPromise);
  });

  $scope.$on('$destroy', function() {
    // Make sure that polling is stopped
    stopPolling();
    
    // Reject deferred scroll to bottom
    scrollBottomPromise && $timeout.cancel(scrollBottomPromise);
    
    if(ionic.Platform.isAndroid()) {
      window.removeEventListener('native.keyboardshow', $scope.inputUp);
      window.removeEventListener('native.keyboardhide', $scope.inputUp);
    }
  });
}