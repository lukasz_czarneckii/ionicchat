angular.module('ionic.utils', [])

.factory('$localstorage', LocalStorage);

LocalStorage.$inject = ['$window'];

function LocalStorage($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return $window.localStorage[key] ? JSON.parse($window.localStorage[key]) : undefined;
    },
    getObjectsWithKeyPrefix: function(prefix) {
      var result = [];
      for(var key in $window.localStorage) {
        if(typeof key === 'string') {
          if(!prefix || key.indexOf(prefix) === 0)
            result.push(JSON.parse($window.localStorage[key]));
        }
      }
      return result;
    },
    remove: function(key) {
      $window.localStorage.removeItem(key);
    },
    clear: function() {
      $window.localStorage.clear();
    },
  };
}

