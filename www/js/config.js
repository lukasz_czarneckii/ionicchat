angular.module('starter')

.constant('PARSE_CONFIG', {
  apiBaseUrl: 'https://api.parse.com/',

  appId: '6GTjBvY7t3rrVpdK8L3i9BDNbJtyTJdZCxKiEo04',
  jsKey: 'QJTroS7lixO70mmVWBJh3uudBwsU1CECkvuGP2VA',
  apiKey: 'DBkfHYSYgpkBXLVISFxpBD8kgApcIXRdjhYimJyL',
  clientKey: 'CfOwaG1snMSXybG7pHI6o54laTPUuI38K6jkQ9TY',
})

.constant('SQLITE_DB_CONFIG', {
  name: 'IonicChatCache.db',
})

.constant('SQLITE_TABLES', {
  messages: 'messages',
  users: 'users',
  chats: 'chats',
})

.constant('LOCAL_STORAGE_KEY_PREFIXES', {
  messages: 'Message.',
  users: 'User.',
  chats: 'Chat.',
});